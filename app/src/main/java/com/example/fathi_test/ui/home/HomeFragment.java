package com.example.fathi_test.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.fathi_test.R;
import com.example.fathi_test.ui.home.adapters.NearByAdapter;
import com.example.fathi_test.ui.home.adapters.RecentAdapter;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView recentRecyclerView;
    private RecyclerView nearbyRecyclerView;
    private RecyclerView.Adapter recentAdapter;
    private RecyclerView.LayoutManager recentLayoutManager;
    private RecyclerView.LayoutManager nearbyLayoutManager;


    private String[] myDataset = {"Dubai Mall", "the emirates", "Ibn Batota", "Splash", "Max", "Mall"};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);


        setHasOptionsMenu(true);


        recentRecyclerView = root.findViewById(R.id.recent_recycler);
        nearbyRecyclerView = root.findViewById(R.id.nearby_recycler);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recentRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        recentLayoutManager = new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false);
        nearbyLayoutManager = new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false);
        recentRecyclerView.setLayoutManager(recentLayoutManager);
        nearbyRecyclerView.setLayoutManager(nearbyLayoutManager);

        // specify an adapter (see also next example)
        recentAdapter = new RecentAdapter(myDataset);
        recentRecyclerView.setAdapter(recentAdapter);

        //for nearby
        nearbyRecyclerView.setAdapter(new NearByAdapter(myDataset));


        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.options_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}