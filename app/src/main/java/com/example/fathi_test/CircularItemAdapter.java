package com.example.fathi_test;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.jh.circularlist.CircularAdapter;

import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

// you should extends CircularAdapter to add your custom item
public class CircularItemAdapter extends CircularAdapter {

    private ArrayList<String> mItems;       // custom data, here we simply use string
    private LayoutInflater mInflater;
    private ArrayList<View> mItemViews;     // to store all list item

    private int testIcons[] = {R.drawable.shop , R.drawable.food , R.drawable.salon};

    public CircularItemAdapter(LayoutInflater inflater, ArrayList<String> items) {
        this.mItemViews = new ArrayList<>();
        this.mItems = items;
        this.mInflater = inflater;

        for (int i=0 ; i<mItems.size() ; i++) {
            View view = mInflater.inflate(R.layout.item_circular, null);
            //view.setBackgroundColor(Color.YELLOW);
//            TextView itemView = (TextView) view.findViewById(R.id.name);
//
//            itemView.setText(s);

            CircleImageView imageView = view.findViewById(R.id.circleImageView);
            imageView.setImageResource(testIcons[i%testIcons.length]);
            mItemViews.add(view);
        }
    }

    @Override
    public ArrayList<View> getAllViews() {
        return mItemViews;
    }

    @Override
    public int getCount() {
        return mItemViews.size();
    }

    @Override
    public View getItemAt(int i) {
        return mItemViews.get(i);
    }

    @Override
    public void removeItemAt(int i) {
        if (mItemViews.size() > 0) {
            // remove from view list
            mItemViews.remove(i);
            // this is necessary to call to notify change
            notifyItemChange();
        }
    }

    @Override
    public void addItem(View view) {
        // add to view list
        mItemViews.add(view);
        // // this is necessary to call to notify change
        notifyItemChange();
    }
}
